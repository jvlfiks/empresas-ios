//
//  empresas_iosTests.swift
//  empresas-iosTests
//
//  Created by João Vitor on 21/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import XCTest
@testable import empresas_ios

class empresas_iosTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDAOLogin() {
        
        let testObject = ["access-token" : "TESTTOKEN",
                          "client" : "TESTCLIENT",
                          "uid" : "TESTUID"]
        
        let result = DAOLogin.convertJSONInLogin(testObject as AnyObject)
        
        XCTAssertEqual(result.client, "TESTCLIENT")
        XCTAssertEqual(result.token, "TESTTOKEN")
        XCTAssertEqual(result.uid, "TESTUID")
        
    }
    
    func testDAOEnterprise() {
        
        let testObject = ["id" : 99,
                          "enterprise_name" : "TESTNAME",
                          "description" : "TESTDESCRIPTION",
                          "photo" : "TESTPHOTO",
                          "city" : "TESTCITY",
                          "country" : "TESTCOUNTRY"] as [String : Any]
        
        let result = DAOEnterprise.convertJSONInEnterprise(testObject as AnyObject)
        
        XCTAssertEqual(result.id, 99)
        XCTAssertEqual(result.name, "TESTNAME")
        XCTAssertEqual(result.description, "TESTDESCRIPTION")
        XCTAssertEqual(result.photo, "TESTPHOTO")
        XCTAssertEqual(result.city, "TESTCITY")
        XCTAssertEqual(result.country, "TESTCOUNTRY")
        
    }

}
