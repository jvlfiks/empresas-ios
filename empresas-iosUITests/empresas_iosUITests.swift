//
//  empresas_iosUITests.swift
//  empresas-iosUITests
//
//  Created by João Vitor on 21/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import XCTest

class empresas_iosUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testValidLogin() {
        
        let validEmail = "testeapple@ioasys.com.br"
        let validPass = "12341234"
        
        let app = XCUIApplication()
        
        let emailTextField = app.scrollViews.children(matching: .textField).element
        XCTAssertTrue(emailTextField.exists)
        emailTextField.tap()
        emailTextField.typeText(validEmail)
        
        let passwordTextField = app.secureTextFields.containing(.button, identifier:"eye").element
        XCTAssertTrue(passwordTextField.exists)
        passwordTextField.tap()
        passwordTextField.typeText(validPass)
        
        app.buttons["ENTRAR"].tap()
        
        let searchField = app.textFields["Busque por empresa"]
        
        expectation(for: NSPredicate(format: "exists == 1"), evaluatedWith: searchField, handler: nil)
        
        waitForExpectations(timeout: 10, handler: nil)
        
        
    }
    
    func testInvalidLogin() {
        
        let invalidEmail = "testeapple@ioasys.com"
        let invalidPass = "123"
        
        let app = XCUIApplication()
        
        let emailTextField = app.scrollViews.children(matching: .textField).element
        XCTAssertTrue(emailTextField.exists)
        emailTextField.tap()
        emailTextField.typeText(invalidEmail)
        
        let passwordTextField = app.secureTextFields.containing(.button, identifier:"eye").element
        XCTAssertTrue(passwordTextField.exists)
        passwordTextField.tap()
        passwordTextField.typeText(invalidPass)
        
        app.buttons["ENTRAR"].tap()
        
        let searchField = app.textFields["Busque por empresa"]
        
        expectation(for: NSPredicate(format: "exists == 0"), evaluatedWith: searchField, handler: nil)
        
        waitForExpectations(timeout: 10, handler: nil)
        
        
    }
    
    func testMissingCredentials(){
        
        let app = XCUIApplication()
        app/*@START_MENU_TOKEN@*/.buttons["ENTRAR"]/*[[".scrollViews.buttons[\"ENTRAR\"]",".buttons[\"ENTRAR\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        let alert = app.alerts["O campo email deve ser preenchido"]
        XCTAssertTrue(alert.exists)
        alert.scrollViews.otherElements.buttons["OK"].tap()
        
    }

    func testLaunchPerformance() {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
                XCUIApplication().launch()
            }
        }
    }
}
