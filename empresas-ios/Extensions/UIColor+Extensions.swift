//
//  UIColor+Extensions.swift
//  empresas-ios
//
//  Created by João Vitor on 21/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import UIKit

extension UIColor{
    
    static func greyText() -> UIColor{
        return UIColor(hex: 0x666666)
    }
    
    static func textFieldBackground() -> UIColor{
        return UIColor(hex: 0xF5F5F5)
    }
    
    static func generalPink() -> UIColor{
        return UIColor(hex: 0xE01E69)
    }
    
    static func wrongRed() -> UIColor{
        return UIColor(hex: 0xE00000)
    }
    
    static func lightBlue() -> UIColor{
        return UIColor(hex: 0x79BBCA)
    }
    
    static func salmon() -> UIColor{
        return UIColor(hex: 0xEB9797)
    }
    
    static func greenFaded() -> UIColor{
        return UIColor(hex: 0x90BB81)
    }
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(hex:Int) {
        self.init(red:(hex >> 16) & 0xff, green:(hex >> 8) & 0xff, blue:hex & 0xff)
    }
    
}
