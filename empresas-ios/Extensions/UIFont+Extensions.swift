//
//  UIFont+Extensions.swift
//  empresas-ios
//
//  Created by João Vitor on 21/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import UIKit

enum FontType : CaseIterable {
    case regular
    case bold
}

extension UIFont {
    
    static var fontName = "Rubik-Regular"
    
    static var boldFontName = "Rubik-SemiBold"
    
    static func appFont(size : CGFloat, type : FontType) -> UIFont {
        
        switch type {
            
        case .regular:
            return UIFont(name: fontName, size: size)!
            
        case .bold:
            return UIFont(name: boldFontName, size: size)!
            
        }
        
    }
    
}
