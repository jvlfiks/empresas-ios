//
//  HomeVC.swift
//  empresas-ios
//
//  Created by João Vitor on 21/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import UIKit

class HomeVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    /* **************************************************************************************************
     **
     **  MARK: Variables Declaration
     **
     ****************************************************************************************************/
    
    var homeView : HomeView!
    
    var enterprises : [Enterprise] = []
    
    var colours : [UIColor] = [UIColor.lightBlue(), UIColor.salmon(), UIColor.greenFaded()]
    
    var currentIndex : Int = 0
    
    /* **************************************************************************************************
     **
     **  MARK: View
     **
     ****************************************************************************************************/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        homeView = HomeView(view: view, parent: self)
        
        homeView.tableView.delegate = self
        homeView.tableView.dataSource = self
        
        homeView.searchField.delegate = self
        
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    /* **************************************************************************************************
    **
    **  MARK: Table View
    **
    ****************************************************************************************************/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enterprises.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = CompanyCell(view: view)
        
        cell.card.backgroundColor = colours[Int.random(in: 0...2)]
        
        cell.selectionStyle = .none
        
        cell.mainImage.image = UIImage(named: "companyImage")
        
        cell.nameLabel.text = enterprises[indexPath.row].name
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = CompanyDetailsVC()
        
        let thisCell = homeView.tableView.cellForRow(at: indexPath) as! CompanyCell
        
        vc.colour = thisCell.card.backgroundColor
        
        vc.enterprise = self.enterprises[indexPath.row]
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    /* **************************************************************************************************
    **
    **  MARK: Text Field Delegate
    **
    ****************************************************************************************************/
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.showLoad(onView: view, backgroundColour: .clear)
        
        EnterpriseAPI.getEnterprise(search: homeView.searchField.text!){(response) in
            
            self.removeLoad()
            
            if response.success{
                
                self.enterprises = response.enterprise
                
                UIView.animate(withDuration: 0.5){
                    
                    self.homeView.foundLabel.text = String(self.enterprises.count) + " resultados encontrados"
                    
                    self.homeView.foundLabel.isHidden = false
                    
                    self.homeView.backgroundImage.frame.size.height = UIScreen.main.bounds.height*0.1
                    
                    self.homeView.searchField.center.y = self.homeView.backgroundImage.frame.origin.y + UIScreen.main.bounds.height*0.1
                    
                    self.homeView.foundLabel.frame.origin.y = self.homeView.searchField.frame.origin.y + self.homeView.searchField.frame.height + 15
                    
                    self.homeView.tableView.frame.origin.y = self.homeView.foundLabel.frame.origin.y + self.homeView.foundLabel.frame.height + 15
                    
                    self.homeView.tableView.frame.size.height = UIScreen.main.bounds.height - self.homeView.tableView.frame.origin.y
                    
                    self.homeView.backgroundImage.image = UIImage(named: "topBar")
                    
                }
                
                if self.enterprises.count == 0{
                    
                    self.homeView.placeholderLabel.isHidden = false
                    
                    self.homeView.foundLabel.isHidden = true
                    
                } else {
                    
                    self.homeView.placeholderLabel.isHidden = true
                    
                }
                
                self.homeView.tableView.reloadData()
                
            }
            
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        homeView.searchField.resignFirstResponder()
        
        return true
        
    }
    
}
