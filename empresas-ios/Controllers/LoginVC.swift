//
//  LoginVC.swift
//  empresas-ios
//
//  Created by João Vitor on 21/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import UIKit

class LoginVC: UIViewController, UITextFieldDelegate {
    
    /* **************************************************************************************************
     **
     **  MARK: Variables Declaration
     **
     ****************************************************************************************************/
    
    var loginView : LoginView!
    
    /* **************************************************************************************************
     **
     **  MARK: View
     **
     ****************************************************************************************************/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginView = LoginView(view: view, parent: self)
        
        loginView.enterButton.addTarget(self, action: #selector(enterAction), for: .touchUpInside)
        
        loginView.togglePassButton.addTarget(self, action: #selector(toggleSecureEntry), for: .touchUpInside)
        
        loginView.emailTextField.delegate = self
        
        loginView.passwordTextField.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    /* **************************************************************************************************
     **
     **  MARK: Enter Action
     **
     ****************************************************************************************************/
    
    @objc func enterAction() {
        
        loginView.emailTextField.resignFirstResponder()
        loginView.passwordTextField.resignFirstResponder()
        
        if loginView.emailTextField.text == nil || loginView.emailTextField.text!.isEmpty {
            
            let alert = UIAlertController(title: "O campo email deve ser preenchido", message: "", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return
            
        }
        
        if loginView.passwordTextField.text == nil || loginView.passwordTextField.text!.isEmpty {
            
            let alert = UIAlertController(title: "O campo senha deve ser preenchido", message: "", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return
            
        }
        
        let params = ["email" : loginView.emailTextField.text!,
                      "password" : loginView.passwordTextField.text!]
        
        self.showLoad(onView: view, backgroundColour: UIColor.black)
        
        UserAPI.signIn(params){(response) in
            
            self.removeLoad()
            
            if response.success{
                
                let vc = UINavigationController(rootViewController: HomeVC())
                
                vc.modalPresentationStyle = .fullScreen
                
                self.present(vc,animated: true,completion: nil)
                
            } else {
                
                self.loginView.wrongCredentialsLabel.isHidden = false
                
                self.loginView.emailTextField.layer.borderColor = UIColor.wrongRed().cgColor
                self.loginView.emailTextField.layer.borderWidth = 1
                
                self.loginView.passwordTextField.layer.borderColor = UIColor.wrongRed().cgColor
                self.loginView.passwordTextField.layer.borderWidth = 1
                
                self.loginView.togglePassButton.setImage(UIImage(named: "wrong"), for: .normal)
                
                self.loginView.wrongEmailImage.isHidden = false
                
            }
            
        }
        
    }
    
    /* **************************************************************************************************
     **
     **  MARK: Text Field Delegate
     **
     ****************************************************************************************************/
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.loginView.wrongCredentialsLabel.isHidden = true
        
        self.loginView.emailTextField.layer.borderColor = UIColor.clear.cgColor
        self.loginView.emailTextField.layer.borderWidth = 1
        
        self.loginView.passwordTextField.layer.borderColor = UIColor.clear.cgColor
        self.loginView.passwordTextField.layer.borderWidth = 1
        
        self.loginView.togglePassButton.setImage(UIImage(named: "eye"), for: .normal)
        
        self.loginView.wrongEmailImage.isHidden = true
    }
    
    /* **************************************************************************************************
     **
     **  MARK: Enter Action
     **
     ****************************************************************************************************/
    
    @objc func toggleSecureEntry() {
        
        loginView.passwordTextField.isSecureTextEntry.toggle()
        
    }
    
}
