//
//  LoggedUser.swift
//  empresas-ios
//
//  Created by João Vitor on 22/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import UIKit

class LoggedUser {
    
    var token : String = ""
    
    var client : String = ""
    
    var uid : String = ""
     
    static let sharedInstance = LoggedUser()
    
    fileprivate init() {
        
        
        
    }
    
    func reset() {
        
        token = ""
        
    }
    
}
