//
//  UserAPI.swift
//  empresas-ios
//
//  Created by João Vitor on 22/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import UIKit
import Alamofire

class UserAPI {
    
    /* **************************************************************************************************
     **
     **  MARK: signIn
     **
     ****************************************************************************************************/
    
    static func signIn(_ params : [String : Any], callback: @escaping (ServerResponse) -> Void) {
        
        let newURL = API.host + API.signin
        
        print ("request - signIn")
        print (newURL)
        print (params)
        
        Alamofire.request(newURL,
                          method : HTTPMethod.post,
                          parameters : params,
                          encoding : URLEncoding.default,
                          headers : nil
        ).responseJSON(completionHandler : { response in
            
            let resposta = ServerResponse()
            
            print ("Response - signIn")
            
            print (response.result.value)
            
            switch response.result {
                
            case let .success(value):
                resposta.statusCode = response.response?.statusCode ?? 0
                
                if response.response?.statusCode == 200 {
                    
                    var loginObject = Login()
                    
                    if let headers = response.response?.allHeaderFields as? [String : Any]{
                        
                        loginObject = DAOLogin.convertJSONInLogin(headers as AnyObject)
                        
                    }
                    
                    LoggedUser.sharedInstance.token = loginObject.token
                    
                    LoggedUser.sharedInstance.client = loginObject.client
                    
                    LoggedUser.sharedInstance.uid = loginObject.uid
                    
                    resposta.success = true
                    
                    callback (resposta)
                    
                    return
                    
                }
                
            case let .failure(error):
                print (error)
                
            }
            
            resposta.success = false
            resposta.errorMessage = "Não foi possível realizar login"
            
            callback (resposta)
            
        })
        
    }
    
    fileprivate init() {
        
        
    }
    
    
}
