//
//  API.swift
//  empresas-ios
//
//  Created by João Vitor on 22/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class API {
    
    static let host = "https://empresas.ioasys.com.br/api/v1/"
    
    static let signin = "users/auth/sign_in"
    
    static let enterprises = "enterprises"
    
    static let sharedInstance = API()
    
    /* *********************************************************************************
     **
     **  MARK: Init
     **
     ***********************************************************************************/
    
    
    fileprivate init() {
        
    }
    
}
