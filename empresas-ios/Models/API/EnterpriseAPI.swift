//
//  EnterpriseAPI.swift
//  empresas-ios
//
//  Created by João Vitor on 22/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import UIKit
import Alamofire

class EnterpriseAPI {
    
    /* *********************************************************************************
    **
    **  MARK: Home Info
    **
    ***********************************************************************************/
    
    static func getEnterprise(search : String,callback: @escaping (ServerResponse) -> Void) {
        
        let newURL = API.host + API.enterprises + "?name=\(search)"
        
        let headers : HTTPHeaders = [
            "access-token": LoggedUser.sharedInstance.token,
            "client" : LoggedUser.sharedInstance.client,
            "uid" : LoggedUser.sharedInstance.uid
        ]
        
        print("resquest - getEnterprise")
        print(newURL)
        print(headers)
        
        Alamofire.request(newURL,
                          method: HTTPMethod.get,
                          parameters: nil,
                          encoding: URLEncoding.default,
                          headers: headers
            ).responseJSON(completionHandler: { response in
                
                let resp = ServerResponse()
                
                print("response - getEnterprise")
                print(response.result)
                
                switch response.result {
                    
                case let .success(value):
                    resp.statusCode = response.response?.statusCode ?? 0
                    
                    if response.response?.statusCode == 200 {
                        
                        let valueObject = value as AnyObject
                        
                        if let info = valueObject["enterprises"] as? [[String : Any]]{
                            
                            resp.enterprise = DAOEnterprise.convertJSONInEnterpriseArray(info as AnyObject)
                            
                        }
                        
                        resp.success = true
                        
                        callback(resp)
                        
                        return
                        
                    }
                    
                case let .failure(error):
                    print(error)
                 
                }
                
                resp.success = false
                resp.errorMessage = "Erro carregando empresas"
                
                callback(resp)
                
            })
        
    }
    
    fileprivate init() {
        
        
    }
    
    
}
