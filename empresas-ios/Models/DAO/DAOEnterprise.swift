//
//  DAOEnterprise.swift
//  empresas-ios
//
//  Created by João Vitor on 22/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class DAOEnterprise {
    
    fileprivate init() {
        
    }
    
    static func convertJSONInEnterpriseArray(_ JSON : AnyObject) -> [Enterprise] {
        
        var array : [Enterprise] = []
        
        guard let data = JSON as? NSArray else {
            return array
        }
        
        for item in data {
            
            array.append(convertJSONInEnterprise(item as AnyObject))
            
        }
        
        return array
        
    }
    
    static func convertJSONInEnterprise(_ JSON : AnyObject) -> Enterprise {
        
        let enterprise = Enterprise()
        
        if let info = JSON["id"] as? Int {
            enterprise.id = info
        }
        
        if let info = JSON["enterprise_name"] as? String {
            enterprise.name = info
        }
        
        if let info = JSON["description"] as? String {
            enterprise.description = info
        }
        
        if let info = JSON["photo"] as? String {
            enterprise.photo = info
        }
        
        if let info = JSON["city"] as? String {
            enterprise.city = info
        }
        
        if let info = JSON["country"] as? String {
            enterprise.country = info
        }
        
        return enterprise
        
    }
    
}
