//
//  DAOLogin.swift
//  empresas-ios
//
//  Created by João Vitor on 22/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class DAOLogin {
    
    fileprivate init() {
        
    }
    
    static func convertJSONInLogin(_ JSON : AnyObject) -> Login {
        
        let login = Login()
        
        if let info = JSON["access-token"] as? String {
            login.token = info
        }
        
        if let info = JSON["client"] as? String {
            login.client = info
        }
        
        if let info = JSON["uid"] as? String{
            login.uid = info
        }
        
        return login
        
    }
    
}
