//
//  Enterprise.swift
//  empresas-ios
//
//  Created by João Vitor on 22/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import UIKit

class Enterprise{
    
    var id : Int = -1
    
    var name : String = ""
    
    var photo : String = ""
    
    var city : String = ""
    
    var country : String = ""
    
    var description : String = ""
    
}
