//
//  CompanyDetailsView.swift
//  empresas-ios
//
//  Created by João Vitor on 21/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import UIKit

class CompanyDetailsView : UIView {
    
    
    /* ************************************************************************
     **
     **  MARK: Variables declaration
     **
     **************************************************************************/
    
    var scrollView: UIScrollView!
    
    var backButton : UIButton!
    
    var companyName : UILabel!
    
    var card : UIView!
    
    var mainImage : UIImageView!
    
    var nameLabel : UILabel!
    
    var detailsLabel : UILabel!
    
    /* ************************************************************************
     **
     **  MARK: Init
     **
     **************************************************************************/
    
    init(view: UIView, parent: UIViewController) {
        super.init(frame: view.frame);
        
        let width = view.frame.size.width
        let height = view.frame.size.height
        
        view.backgroundColor = UIColor.white
        
        var yPosition = CGFloat (height*0.1)
        
        //------------------------- Scroll View -----------------------------
        
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        scrollView.isScrollEnabled = true
        scrollView.backgroundColor = .clear
        
        view.addSubview(scrollView)
        
        //--------------------------Back Button------------------------------
        
        backButton = UIButton(frame: CGRect(x: width*0.05, y: 10, width: 50, height: 50))
        backButton.setImage(UIImage(named: "back")?.withTintColor(UIColor.generalPink()), for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        backButton.backgroundColor = .textFieldBackground()
        backButton.layer.cornerRadius = 5
        
        scrollView.addSubview(backButton)
        
        //-------------------------Company Label-----------------------------
        
        companyName = UILabel(frame: CGRect(x: width*0.175, y: 0, width: width*0.65, height: 30))
        companyName.center.y = backButton.center.y
        companyName.textAlignment = .center
        companyName.font = UIFont.appFont(size: 22, type: .bold)
        
        scrollView.addSubview(companyName)
        
        yPosition = backButton.frame.origin.y + backButton.frame.height + 5
        
        //--------------------------Card----------------------------
        
        card = UIView(frame: CGRect(x: 0, y: yPosition + 15, width: width, height: 135))
        card.backgroundColor = .generalPink()
        
        scrollView.addSubview(card)
        
        
        //------------------------Main Image-------------------------
        
        mainImage = UIImageView(frame: CGRect(x: 0, y: yPosition + 45, width: 35, height: 35))
        mainImage.contentMode = .scaleAspectFit
        mainImage.center.x = width/2
        
        scrollView.addSubview(mainImage)
        
        //------------------------Name Label-------------------------
        
        nameLabel = UILabel(frame: CGRect(x: width*0.05, y: yPosition + 85, width: width*0.9, height: 30))
        nameLabel.textColor = .white
        nameLabel.textAlignment = .center
        nameLabel.font = UIFont.appFont(size: 25, type: .bold)
        
        scrollView.addSubview(nameLabel)
        
        yPosition = card.frame.origin.y + card.frame.height + 15
        
        //------------------------Name Label-------------------------
        
        detailsLabel = UILabel(frame: CGRect(x: width*0.05, y: yPosition, width: width*0.9, height: height*0.4))
        detailsLabel.textColor = .greyText()
        detailsLabel.font = UIFont.appFont(size: 22, type: .regular)
        detailsLabel.numberOfLines = 0
        detailsLabel.lineBreakMode = .byWordWrapping
        
        scrollView.addSubview(detailsLabel)
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
