//
//  LoginView.swift
//  empresas-ios
//
//  Created by João Vitor on 21/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import UIKit

class LoginView : UIView {
    
    
    /* ************************************************************************
     **
     **  MARK: Variables declaration
     **
     **************************************************************************/
    
    var scrollView: UIScrollView!
    
    var backgroundImage : UIImageView!
    
    var logoView : UIImageView!
    
    var welcomeLabel : UILabel!
    
    var emailLabel : UILabel!
    
    var emailTextField : UITextField!
    
    var wrongEmailImage : UIButton!
    
    var passwordLabel : UILabel!
    
    var passwordTextField : UITextField!
    
    var wrongCredentialsLabel : UILabel!
    
    var togglePassButton : UIButton!
    
    var enterButton : UIButton!
    
    /* ************************************************************************
     **
     **  MARK: Init
     **
     **************************************************************************/
    
    init(view: UIView, parent: UIViewController) {
        super.init(frame: view.frame);
        
        let width = view.frame.size.width
        let height = view.frame.size.height
        
        view.backgroundColor = UIColor.white
        
        var yPosition = CGFloat (height*0.1)
        
        //------------------------- Scroll View -----------------------------
        
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        scrollView.isScrollEnabled = true
        scrollView.backgroundColor = .clear
        
        view.addSubview(scrollView)
        
        //------------------------Background Image---------------------------
        
        backgroundImage = UIImageView(frame: CGRect(x: 0, y: -width*0.35, width: width*1.45, height: width))
        backgroundImage.image = UIImage(named: "loginBackground")
        backgroundImage.center.x = width/2
        backgroundImage.contentMode = .scaleToFill
        backgroundImage.round(corners: [.bottomLeft, .bottomRight], radius: backgroundImage.frame.height/2)
        
        scrollView.addSubview(backgroundImage)
        
        //---------------------------Logo View--------------------------------
        
        logoView = UIImageView(frame: CGRect(x: 0, y: yPosition, width: 45, height: 45))
        logoView.image = UIImage(named: "loginLogo")
        logoView.center.x = width/2
        logoView.contentMode = .scaleAspectFit
        
        scrollView.addSubview(logoView)
        
        yPosition = yPosition + logoView.frame.height + 5
        
        //-------------------------Welcome Label------------------------------
        
        welcomeLabel = UILabel(frame: CGRect(x: width*0.05, y: yPosition, width: width*0.9, height: 40))
        welcomeLabel.textAlignment = .center
        welcomeLabel.text = "Seja bem vindo ao empresas!"
        welcomeLabel.textColor = .white
        welcomeLabel.font = UIFont.appFont(size: 24, type: .regular)
        
        scrollView.addSubview(welcomeLabel)
        
        yPosition = backgroundImage.frame.origin.y + backgroundImage.frame.height + height*0.05
        
        //--------------------------Email Label------------------------------
        
        emailLabel = UILabel(frame: CGRect(x: width*0.05, y: yPosition, width: width*0.9, height: 30))
        emailLabel.text = "Email"
        emailLabel.textColor = .greyText()
        emailLabel.font = UIFont.appFont(size: 16, type: .regular)
        
        scrollView.addSubview(emailLabel)
        
        yPosition = yPosition + emailLabel.frame.height
        
        //------------------------Email Field-----------------------------
        
        emailTextField = UITextField(frame: CGRect(x: width*0.05, y: yPosition, width: width*0.9, height: 60))
        emailTextField.layer.cornerRadius = 10
        emailTextField.backgroundColor = UIColor.textFieldBackground()
        emailTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: emailTextField.frame.height))
        emailTextField.leftViewMode = .always
        emailTextField.keyboardType = .emailAddress
        emailTextField.autocorrectionType = .no
        emailTextField.autocapitalizationType = .none
        emailTextField.tintColor = UIColor.generalPink()
        
        wrongEmailImage = UIButton(type: .custom)
        wrongEmailImage.setImage(UIImage(named: "wrong"), for: .normal)
        wrongEmailImage.imageEdgeInsets = UIEdgeInsets(top: 13, left: 20, bottom: 13, right: 20)
        wrongEmailImage.imageView?.contentMode = .scaleAspectFit
        wrongEmailImage.frame = CGRect(x: CGFloat(emailTextField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        emailTextField.rightView = wrongEmailImage
        emailTextField.rightViewMode = .always
        
        wrongEmailImage.isHidden = true
        
        scrollView.addSubview(emailTextField)
        
        yPosition = yPosition + emailTextField.frame.height + 20
        
        //-----------------------Password Label---------------------------
        
        passwordLabel = UILabel(frame: CGRect(x: width*0.05, y: yPosition, width: width*0.9, height: 30))
        passwordLabel.text = "Senha"
        passwordLabel.textColor = .greyText()
        passwordLabel.font = UIFont.appFont(size: 16, type: .regular)
        
        scrollView.addSubview(passwordLabel)
        
        yPosition = yPosition + passwordLabel.frame.height
        
        //----------------------Password Field---------------------------
        
        passwordTextField = UITextField(frame: CGRect(x: width*0.05, y: yPosition, width: width*0.9, height: 60))
        passwordTextField.layer.cornerRadius = 10
        passwordTextField.backgroundColor = UIColor.textFieldBackground()
        passwordTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: passwordTextField.frame.height))
        passwordTextField.leftViewMode = .always
        passwordTextField.isSecureTextEntry = true
        passwordTextField.autocorrectionType = .no
        passwordTextField.autocapitalizationType = .none
        passwordTextField.tintColor = UIColor.generalPink()
        
        scrollView.addSubview(passwordTextField)
        
        togglePassButton = UIButton(type: .custom)
        togglePassButton.setImage(UIImage(named: "eye"), for: .normal)
        togglePassButton.imageEdgeInsets = UIEdgeInsets(top: 13, left: 20, bottom: 13, right: 20)
        togglePassButton.imageView?.contentMode = .scaleAspectFit
        togglePassButton.frame = CGRect(x: CGFloat(passwordTextField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        passwordTextField.rightView = togglePassButton
        passwordTextField.rightViewMode = .always
        
        yPosition = yPosition + passwordTextField.frame.height + 5
        
        //-----------------------Password Label---------------------------
        
        wrongCredentialsLabel = UILabel(frame: CGRect(x: width*0.05, y: yPosition, width: width*0.9, height: 20))
        wrongCredentialsLabel.text = "Credenciais incorretas"
        wrongCredentialsLabel.textAlignment = .right
        wrongCredentialsLabel.textColor = .wrongRed()
        wrongCredentialsLabel.font = UIFont.appFont(size: 14, type: .regular)
        wrongCredentialsLabel.isHidden = true
        
        scrollView.addSubview(wrongCredentialsLabel)
        
        yPosition = yPosition + wrongCredentialsLabel.frame.height + 30
        
        //-----------------------Enter Button--------------------------
        
        enterButton = UIButton(frame: CGRect(x: width*0.15, y: yPosition, width: width*0.7, height: 60))
        enterButton.setTitle("ENTRAR", for: .normal)
        enterButton.titleLabel?.font = UIFont.appFont(size: 20, type: .bold)
        enterButton.setTitleColor(UIColor.white, for: .normal)
        enterButton.layer.cornerRadius = 15
        enterButton.backgroundColor = UIColor.generalPink()
        
        scrollView.addSubview(enterButton)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
